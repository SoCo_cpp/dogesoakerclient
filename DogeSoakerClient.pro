#-------------------------------------------------
#
# Project created by QtCreator 2015-01-04T02:44:39
#
#-------------------------------------------------

QT       += core network
QT       -= gui

CONFIG   += console
CONFIG   -= app_bundle

TARGET = DogeSoakerClient

TEMPLATE = app

INCLUDEPATH += /usr/include/python2.7
LIBS += -lpython2.7

MLIB_SRC = ../mlib
MLIB_CONFIG = file logging comm irc unixsignals threads
include( $$MLIB_SRC/mlib.pro )

SOURCES += src/main.cpp \
           src/dogesoaker/dogesoaker.cpp \
           src/dogesoaker/dogesoakerirchandler.cpp \
           src/dogesoaker/embedirc.cpp \
           src/dogesoaker/dogesoakerconfigfile.cpp

HEADERS += src/dogesoaker/dogesoaker.h \
           src/dogesoaker/dogesoakerirchandler.h \
           src/dogesoaker/embedirc.h \
           src/dogesoaker/dogesoakerconfigfile.h
