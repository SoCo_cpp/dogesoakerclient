Doge Soaker Client

Qt C++ headless IRC client that specifically runs the Doge Soaker plugin.
Required dependencies:

    Qt development environment. Currently optimized for Qt5, but moving back to Qt4 should be trivial.
    MLib library: https://bitbucket.org/SoCo_cpp/mlib
    Doge Soaker plugin: https://bitbucket.org/SoCo_cpp/doge-soaker

Configure DogeSoakerClient.pro:

    Set MLIB_SRC with the base path to the MLib library.
    Configure INCLUDEPATH and LIBS with Python include/library entries.

Compiling

This compiling procedure not tested on Windows and is generally targeted to Linux.

Move to a working directory and clone the DogeSoakerClient and MLib projects in the same parent directory:

    git clone https://SoCo_cpp@bitbucket.org/SoCo_cpp/dogesoakerclient.git
    git clone https://SoCo_cpp@bitbucket.org/SoCo_cpp/mlib.git

MLIB_SRC defaults to ../mlib so you shouldn't need to change that unless you want the projects in different locations. 

Install Python, which may need the development package such as python-dev. Ensure that QMake can find Python's include/library directories, by modifying DogeSoakerClient.pro to add the appropriate paths to INCLUDEPATH and LIBS. Particularly Python.h and pythread.h is needed.

From there, compiling should be as simple as changing to the DogeSoakerClient directory and running:

    qmake
    make

The DogeSoakerClient project will compile in the MLib parts, so separately compiling MLib isn't needed. The DogeSoakerClient (or DogeSoakerClient.exe) executable should have been generated.

Running DogeSoakerClient requires the Python bot script you want, such as dogesoaker.py, to be found as a module by Python. To ensure it is found, add the script's directory to PYTHONPATH. It is easiest to make a script that exports PYTHONPATH with the directory and then execute's DogeSoakerClient using the screen command if you are in a headless environment. DogeSoakerClient will produce a skeleton config file on it's first run that you will need to fill out with IRC credentials and proxy information, then you can run it again.



Usage

    Review the Doge Soaker plugin script's options and configurations at the top of the source file dogesoaker.py. Configure with C_XChat_Plugin set to False.
    Execute DogeSoakerClient to produce a skeleton IRC client configuration file and fill in with IRC connection and login options or create your own at ~/.dogesoaker/dogesoaker.config.
    Ensure environment variable PYTHONPATH includes dogesoaker.py's path so DogeSoakerClient can find the Python module.
    Execute DogeSoakerClient and use output to ensure it loads the Python module, connects, and logs in the IRC user properly.
    Join one or more supported tipbot enabled IRC channels, by using the appropriate operator commands.
    Verify operation.

