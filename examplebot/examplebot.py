
import embirc # export-module-name

##############################################
# Define bot's class, ExampleBox             #
##############################################

class ExampleBot:
	nick = "Guest"
	def __init__(self):
		# constructor
		pass #@TODO
		
	def destructor(self):
		# destructor
		pass #@TODO
		
	def handlePrivMsg(self, fromUser, toUser, message):
		# private message
		message = message.lower(); # lowercase
		if message.startswith("!help"):
			embirc.PrivMsg(fromUser, "DogeSoakerClient example bot")
			embirc.PrivMsg(fromUser, "   !reload - reload bot python module")
			embirc.PrivMsg(fromUser, "   !reconnect - reconnect irc client")
			embirc.PrivMsg(fromUser, "   !wow (in channel) - Says, \"Wow User!\"")
		elif message.startswith("!reload"):
			embirc.PrivMsg(fromUser, "Reloading...")
			embirc.ClientCommand("reload")
		elif message.startswith("!reconnect"):
			embirc.PrivMsg(fromUser, "Reconnecting...")
			embirc.ClientCommand("reconnect")
		
	def handleChannelMessage(self, channel, user, message):
		# channel message
		message = message.lower(); # lowercase
		if message.startswith("!help"):
			embirc.ChanMsg(channel, "DogeSoakerClient example bot")
		elif message.startswith("!wow"):
			embirc.ChanMsg(channel, "Wow " + user + "!")
			
			
	def handleWhoisAuth(self, currentNick, authNick):
		# Whois auth part
		pass #@TODO
		
	def handleWhoisName(self, nick, user, host, realName):
		# whois name line
		pass #@TODO
		
	def handleWhoisEnd(self, nick):
		# whois end (have all whois for user)
		pass #@TODO
		
	def handleJoin(self, nick, host, channel):
		# a user joined the channel (you or another user)
		pass #@TODO
		
	def handlePart(self, nick, host, channel, reason):
		# a user parted a channel
		pass #@TODO
		
	def handleQuit(self, nick, host, reason):
		# a user quit
		pass #@TODO
		
	def handleNickChanged(self, oldNick, host, newNick):
		# a user changed her nick
		pass #@TODO
		
	def handleKicked(self, nick, channel, reason, kicker):
		# a user was kicked	
		pass #@TODO

##############################################
# create main instance of ExampleBotclass	 #
##############################################

bot = ExampleBot()

##############################################
# Connect exported module functions 		 #
# to main instance of ExampleBot class	     #
##############################################

def DS_Init():
	global bot
	bot.Nick = embirc.GetNick()
	
def DS_Unload():
	global bot
	bot.destructor()	
	
def DS_HandlePrivMsg(fromUser, toUser, message):
	global bot
	bot.handlePrivMsg(fromUser, toUser, message)
	
def DS_HandleChannelMessage(channelName, userName, message):
	global bot
	bot.handleChannelMessage(channelName, userName, message)
	
def DS_HandleWhoisAuth(currNick, authNick):
	global bot
	bot.handleWhoisAuth(currNick, authNick)
	
def DS_HandleWhoisNameLine(nick, hostUser, host, realName):
	global bot
	bot.handleWhoisName(nick, hostUser, host, realName)
	
def DS_HandleWhoisEnd(nick):
	global bot
	bot.whoisEnd(nick)
	
def DS_HandleJoin(nick, host, channel):
	global bot
	bot.handleJoin(nick, host, channel)
	
def DS_HandlePart(nick, host, channelName, reason):
	global bot
	bot.handlePart(nick, host, channelName, reason)
	
def DS_HandleQuit(nick, host, reason):
	global bot
	bot.handleQuit(nick, host, reason)
	
def DS_HandleNickChanged(oldNick, host, newNick):
	global bot
	bot.handleNickChanged(oldNick, host, newNick)
	
def DS_HandleKick(nick, channelName, reason, kickerNick):
	global bot
	bot.handleKick(nick, channelName, reason, kickerNick)
	
