#include <QCoreApplication>
#include <QDebug>
#include "src/dogesoaker/dogesoaker.h"

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);

	if (!DogeSoaker::instance().start(&a))
	{
		qDebug() << "DogeSoaker failed to start";
		//return 1;
	}
	return a.exec();
}
