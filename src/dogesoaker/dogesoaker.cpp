#include <QCoreApplication>
#include <QDir>
#include <QFile>
#include <QDateTime>
#include "src/dogesoaker/dogesoaker.h"
#include "logging/debuglogger.h"
#include "src/dogesoaker/dogesoakerconfigfile.h"

DogeSoaker::DogeSoaker() :
	UnixSignalHandler(0),
	_botMode(mStop),
	_botStatus(bNone),
	_connectionStatus(cNone),
	_irc(),
	_ircLogger(_irc),
	_ircRawLogger(_irc),
	_ircHandler(*this),
    _recoverNickStep(0),
    _badLagCount(0),
    _lagLimitBadLag(0),
    _lagLimitBadMax(0),
    _lagLimitMaxLag(0)

{
    _versionString = QString("%1.%2").arg(C_DogeSoaker_Version_Major).arg(C_DogeSoaker_Version_Minor);
	connect(&_irc, SIGNAL(clientStatusChanged()), this, SLOT(subsystemStatusChanged()));
	connect(&_irc, SIGNAL(loginStatusChanged()), this, SLOT(subsystemStatusChanged()));
	connect(&_statusTimer, SIGNAL(timeout()), this, SLOT(statusTimerTick()));
    connect(&_irc, SIGNAL(watchdogError(QString)), this, SLOT(ircWatchdogError(QString)));
    connect(&_irc, SIGNAL(lagAverage(double)), this, SLOT(lagAverage(double)));
    _statusTimer.start(cNotReadyStatusTimerInterval);
}

DogeSoaker::~DogeSoaker()
{
	_ircHandler.release();
}

/*static*/ DogeSoaker& DogeSoaker::instance()
{
	static DogeSoaker instance;
	return instance;
}

const IrcClient& DogeSoaker::irc() const
{
	return _irc;
}

IrcClient& DogeSoaker::irc()
{
	return _irc;
}

void DogeSoaker::setLagLimits(int badLag, int badMax, int maxLag)
{
    _lagLimitBadLag = badLag;
    _lagLimitBadMax = badMax;
    _lagLimitMaxLag = maxLag;
}

int DogeSoaker::lagLimitBadLag() const
{
    return _lagLimitBadLag;
}

int DogeSoaker::lagLimitBadMax() const
{
    return _lagLimitBadMax;
}

int DogeSoaker::lagLimitMaxLag() const
{
    return _lagLimitMaxLag;
}

QString DogeSoaker::dataPath() const
{
	return QString("%1/.%2").arg(QDir::homePath()).arg(C_DogeSoaker_SystemNameShort);
}

QString DogeSoaker::ircLogPath() const
{
	return QString("%1/%2/%3").arg(dataPath()).arg(C_DogeSoaker_IrcLog_Dir).arg(_irc.config().accountConfig().nick());
}


QString DogeSoaker::debugLogFilename() const
{
	return QString("%1/%2").arg(dataPath()).arg(C_DogeSoaker_DebugLog_Filename);
}

QString DogeSoaker::configFilename() const
{
	return QString("%1/%2").arg(dataPath()).arg(C_DogeSoaker_Config_Filename);
}

bool DogeSoaker::start(QCoreApplication* pApp)
{
	Q_ASSERT(pApp);
	_pApp = pApp;

	if (initUnitSignalHandlers() != 0)
		qFatal("DogeSoaker::start initUnitSignalHandlers failed");

	connect(_pApp, SIGNAL(aboutToQuit()), this, SLOT(applicationAboutToQuit()));

	if (!initDebugLog())
	{
		qDebug() << C_DogeSoaker_SystemName << " initDebugLog failed";
		return false;
	}
	DebugLog(dlNone, "------- %s %s -------", qPrintable(C_DogeSoaker_SystemName), qPrintable(_versionString));

	if (!loadConfig())
	{
		DebugLog(dlError, "DogeSoaker::start loadConfig failed");
		return false;
	}

    qDebug() << "\nIRC Client: " << C_DogeSoaker_SystemName << " " << _versionString;
    qDebug() << "IRC client      data path: " << dataPath();
    qDebug() << "IRC client    config file: " << configFilename();
    qDebug() << "IRC client       log path: " << ircLogPath();
    qDebug() << "IRC client debug log file: " << debugLogFilename();
    qDebug() << "IRC client     lag limits: bad " << _lagLimitBadLag << "ms (count "<< _lagLimitBadMax << "), max " << _lagLimitMaxLag << "ms\n";
    qDebug() << "Bot                module: " << ircHandler().botModuleName();
    qDebug() << "Bot                export: " << ircHandler().exportModuleName() << "\n";

    if (!initIrcLogging())
	{
		DebugLog(dlError, "DogeSoaker::start initIrcLogging failed");
		return false;
	}

    if (!_ircHandler.init())
    {
        DebugLog(dlError, "DogeSoaker::start ircHandler init failed");
        return false;
    }

    if (!_irc.enableWatchdog())
    {
        DebugLog(dlError, "DogeSoaker::start IrcClient enableWatchdog failed");
        return false;
    }
	qDebug() << C_DogeSoaker_SystemName << "started.";
    setBotMode(mStart);
    //test();
	return true;
}

void DogeSoaker::unixSignalEvent(int sigNum) // inherited from UnixSignalHandler
{
	DebugLog(dlWarn, "DogeSoaker::unixSignalEvent going down with signal %d", sigNum);
	qDebug() << "\nDogeSoaker::unixSignalEvent going down with signal " << sigNum;
	QCoreApplication::exit(0);
}

bool DogeSoaker::initDebugLog()
{
	DebugLogInstance().setFileName(debugLogFilename());
    DebugLogInstance().setArchiveEnabled();
    DebugLogInstance().setArchiveDir("archive", true /*appendToFilePath*/);
    if (!DebugLogInstance().makePath())
	{
		qDebug() << C_DogeSoaker_SystemName << " Failed to make data path for debug log: " << debugLogFilename();
		return false;
	}
	return true;
}

bool DogeSoaker::initIrcLogging()
{
	_ircLogger.setAllLogOptions();
	_ircLogger.setBaseLogPath(ircLogPath());
	_ircLogger.setArchiving(250000);
	_ircLogger.setEnabled();

	_ircRawLogger.setFileName(ircLogPath() + "/" + C_DogeSoaker_IrcLog_RawLog_FileName);
	_ircRawLogger.setArchiving(250000);

	if (!_ircRawLogger.logFile()->makePath())
	{
		DebugLog(dlError, "DogeSoaker::initIrcLogging failed to make paths for raw IRC logger");
		return false;
	}
	return true;
}

void DogeSoaker::applicationAboutToQuit()
{
    DebugLog(dlWarn, "DogeSoaker::applicationAboutToQuit...");
    debugDumpStatus();
    _irc.disconnectClient("Closing client", true/*force*/);
	setBotMode(mStop);
	qDebug() << "DogeSoakerClient quitting...";
}

void DogeSoaker::setBotMode(BotMode botMode)
{
	_botMode = botMode;
	emit botModeChanged();
}

const DogeSoaker::BotMode& DogeSoaker::botMode() const
{
	return _botMode;
}

QString DogeSoaker::botModeString() const
{
	switch (_botMode)
	{
		case mStop:		return "Stop";
		case mStart:	return "Start";
		case mPause:	return "Pause";
		default:		return QString("Unknown(%1)").arg(_botMode);
	} // switch
}

void DogeSoaker::setConnectionStatus(ConnectionStatus connectionStatus)
{
	_connectionStatus = connectionStatus;
	emit connectionStatusChanged();
}

const DogeSoaker::ConnectionStatus& DogeSoaker::connectionStatus() const
{
	return _connectionStatus;
}

QString DogeSoaker::connectionStatusString() const
{
	switch (_connectionStatus)
	{
		case cNone:				return "None";
		case cDisconnecting:	return "Disconnecting";
		case cLostConnection:	return "LostConnection";
		case cDisconnected:		return "Disconnected";
		case cConnecting:		return "Connecting";
		case cConnected:		return "Connected";
		case cLogin:			return "Login";
		case cReady:			return "Ready";
		default:				return QString("Unknown(%1)").arg(_connectionStatus);
	} // switch
}

void DogeSoaker::setBotStatus(BotStatus botStatus)
{
	_botStatus = botStatus;
	emit botStatusChanged();
}

const DogeSoaker::BotStatus& DogeSoaker::botStatus() const
{
	return _botStatus;
}

QString DogeSoaker::botStatusString() const
{
	switch (_botStatus)
	{
		case bNone:				return "None";
		case bWaitingConnect:	return "WaitingConnect";
		case bConnected:		return "Connected";
		case bConnectionLost:	return "ConnectionLost";
		case bReady:			return "Ready";
		case bError:			return "Error";
		default:				return QString("Unknown(%1)").arg(_botStatus);
	} // switch
}

bool DogeSoaker::isConnected() const
{
	switch (_connectionStatus)
	{
		case cConnected:
		case cLogin:
		case cReady:
			return true;
		default:
			return false;

	} // switch (_clientStatus)
}

bool DogeSoaker::isDisconnected(bool bFullDisconnect /*= false*/) const
{
	switch (_connectionStatus)
	{
		case cNone:
		case cDisconnected:
			return true;
		case cDisconnecting:
			return !bFullDisconnect;
		default:
			return false;
	} // switch (_clientStatus)
}

bool DogeSoaker::isReady() const
{
	return (_botStatus == bReady);
}

void DogeSoaker::start()
{
	setBotMode(mStart);
}

void DogeSoaker::stop()
{
	setBotMode(mStop);
}

void DogeSoaker::pause()
{
	setBotMode(mPause);
}

void DogeSoaker::resume()
{
	setBotMode(mStart);
}

void DogeSoaker::reconnect()
{
	if (!_irc.connectClient(true/*forceReconnect*/))
	{
		DebugLog(dlWarn, "DogeSoaker::reconnect irc client failed to connectClient with forceReconnect, disconnecting");
		if (!_irc.disconnectClient())
		{
            DebugLog(dlWarn, "DogeSoaker::reconnect irc client failed to dissconnectClient, stopping bot!");
			setBotMode(mStop);
		}
	}
}

void DogeSoaker::handleNickInUse()
{
	QString tmpNick = QString("dstmp%1").arg(QDateTime::currentMSecsSinceEpoch());
	DebugLog(dlDebug, "DogeSoaker::handleNickInUse starting nickRecoveryTimer tmpNick: %s", qPrintable(tmpNick));
	emit _irc.sendNickChange(tmpNick);
    emit _ircHandler.receivedIrcHanlderNick("*", "*", tmpNick);
    _recoverNickStep = 0;
	_recoverNickTimer.singleShot(10, this, SLOT(nickRecoveryTimerTick()));
}

void DogeSoaker::nickRecoveryTimerTick()
{
	DebugLog(dlDebug, "DogeSoaker::nickRecoveryTimerTick step %d", _recoverNickStep);
	if (!isConnected())
	{
		DebugLog(dlDebug, "DogeSoaker::nickRecoveryTimerTick not connecting, aborting");
		return;
	}
    if (_irc.loginStatus() != IrcClient::lReady)
	{
		_recoverNickTimer.singleShot(200, this, SLOT(nickRecoveryTimerTick()));
		return;
	}
	switch (_recoverNickStep)
	{
		case 0:
			emit _irc.sendNickGHost();
			_recoverNickStep++;
			_recoverNickTimer.singleShot(2000, this, SLOT(nickRecoveryTimerTick()));
			break;
		case 1:
			emit _irc.sendNickRelase();
			_recoverNickStep++;
			_recoverNickTimer.singleShot(2000, this, SLOT(nickRecoveryTimerTick()));
			break;
		case 2:
			emit _irc.sendNickChange(""); // empty uses configured nick
			_recoverNickStep = 0;
			break;
	} // switch (_recoverNickStep)
}

bool DogeSoaker::loadConfig()
{
	DogeSoakerConfigFile cfgFile;
	cfgFile.setFileName(configFilename());

	if (!cfgFile.fileExists())
	{
		DebugLog(dlWarn, "DogeSoaker::loadConfig config file doesn't exist, loading defaults, saving, and quitting");
		defaultConfig();
		if (!saveConfig())
			DebugLog(dlWarn, "DogeSoaker::loadConfig saveConfig failed");
		return false;
	}
	if (!cfgFile.load())
	{
		DebugLog(dlWarn, "DogeSoaker::loadConfig cfgFile load failed");
		return false;
	}
	_irc.config().serverConfig().setType(CompositeConnectionConfig::ctTcp);
	_irc.config().serverConfig().setServer(false);
	qDebug() << "config file loaded";
	return true;
}

bool DogeSoaker::saveConfig()
{
	DogeSoakerConfigFile cfgFile;
	cfgFile.setFileName(configFilename());
	if (!cfgFile.save())
	{
		DebugLog(dlWarn, "DogeSoaker::saveConfig cfgFile save failed");
		return false;
	}
	return true;
}

void DogeSoaker::defaultConfig()
{
	_irc.config() = IrcClientConfig();
	_irc.config().accountConfig().set("Required_IRC_Nick", "Required_IRC_User", "Required_IRC_RealName", "Required_IRC_NickservPassword");
	_irc.config().serverConfig().setTcp("Required_IRC_Host", 6667);
}

void DogeSoaker::setRawIrcLogEnabled(bool enabled /*= true*/)
{
	_ircRawLogger.setEnabled(enabled);
}

bool DogeSoaker::isRawIrcLogEnabled() const
{
	return _ircRawLogger.isEnabled();
}

void DogeSoaker::debugDumpStatus()
{
    DebugLog(dlDebug, "DogeSoaker::debugDumpStatus:");
    DebugLog(dlDebug, " - version %s", qPrintable(_versionString));
    DebugLog(dlDebug, " - connection %s:%d via proxy: %s:%d", qPrintable(_irc.config().serverConfig().hostName()), (int)_irc.config().serverConfig().hostPort(), qPrintable(_irc.config().serverConfig().proxyConfig().hostName()), (int)_irc.config().serverConfig().proxyConfig().hostPort());
    DebugLog(dlDebug, " - bot mode: %s, bot status: %s, client status %s, login status %s, conection status: %s", qPrintable(botModeString()), qPrintable(botStatusString()), qPrintable(_irc.clientStatusString()), qPrintable(_irc.loginStatusString()), qPrintable(connectionStatusString()));
    DebugLog(dlDebug, " - last received %lds, last ready %lds", (long)_irc.handler().lastReceived().secsTo(QDateTime::currentDateTime()), (long)_irc.lastReady().secsTo(QDateTime::currentDateTime()));
    DebugLog(dlDebug, " - last received packet: %s", qPrintable(_irc.handler().packet().packet()));
}

void DogeSoaker::subsystemStatusChanged()
{
    //DebugLog(dlDebug, "DogeSoaker::subsystemStatusChanged irc status: %s irc login: %s", qPrintable(_irc.clientStatusString()), qPrintable(_irc.loginStatusString()));
	if (_irc.isConnected())
	{

        //DebugLog(dlDebug, "DogeSoaker::subsystemStatusChanged all connected, Irc ready: %c", BoolYN(_irc.isReady()));
		if (_irc.isReady())
		{
			setConnectionStatus(cReady);
			setBotStatus(bConnected);
		}
		else
			setConnectionStatus(cConnected);
	}
	else
	{
		if (_irc.isDisconnected())
		{

			setBotStatus(bNone);
		}
		else
		{
			if ((_connectionStatus != cConnecting && _connectionStatus != cDisconnecting) && _irc.isDisconnected() )
				setConnectionStatus(cLostConnection);
			setBotStatus(bWaitingConnect);
		}
	}
}

void DogeSoaker::statusTimerTick()
{
	checkMode();
	if (!isReady())
	{
		switch (_botStatus)
		{
			case bConnected:
				//_ircHandler.DSInit();
				setBotStatus(bReady);

			default:
				break;
		} // switch (_botStatus)

	}
	if (_statusTimer.interval() != cNotReadyStatusTimerInterval)
		_statusTimer.start(cNotReadyStatusTimerInterval);
	//debugDumpStatus();
}

void DogeSoaker::checkMode()
{
	if (isDisconnected())
	{
		if (_botMode == mStart || _botMode == mPause)
		{
			if (_connectionStatus != cLostConnection)
			{
				setConnectionStatus(cConnecting);
				setBotStatus(bWaitingConnect);
				qDebug() << "Connecting to: " << qPrintable(_irc.config().serverConfig().hostName()) << ":" << (int)_irc.config().serverConfig().hostPort() << " via proxy: " << _irc.config().serverConfig().proxyConfig().hostName() << ":" << _irc.config().serverConfig().proxyConfig().hostPort();
				if (!_irc.connectClient())
				{
					DebugLog(dlWarn, "DogeSoaker::checkMode irc client connectClient failed");
					setBotStatus(bError);
				}
			}
		}
	}
	else if (isConnected())
	{
		if (_botMode == mStop)
		{
			DebugLog(dlWarn, "DogeSoaker::checkMode disconnecting...");
			if (!_irc.disconnectClient("Stopping"))
				DebugLog(dlWarn, "DogeSoaker::checkMode irc client disconnectClient failed");

		}
	}
}

const QString& DogeSoaker::embGetNick()
{
	//DebugLog(dlDebug, "DogeSoaker::embGetNick Nick: %s", qPrintable(_irc.config().accountConfig().nick()));
	return _irc.config().accountConfig().nick();
}

void DogeSoaker::embChanMsg(const QString& sChannelName, const QString& sMessage)
{
	//DebugLog(dlDebug, "DogeSoaker::embChanMsg");
	emit _irc.sendPrivateMessage(sChannelName, sMessage);
}

void DogeSoaker::embPrivMsg(const QString& sToNick, const QString& sMessage)
{
	//DebugLog(dlDebug, "DogeSoaker::embPrivMsg");
	emit _irc.sendPrivateMessage(sToNick, sMessage);
}

void DogeSoaker::embCommand(const QString& sMessage)
{
	//DebugLog(dlDebug, "DogeSoaker::embCommand");
	emit _irc.send(sMessage);
}

DogeSoakerIrcHandler& DogeSoaker::ircHandler()
{
	return _ircHandler;
}

const DogeSoakerIrcHandler& DogeSoaker::ircHandler() const
{
	return _ircHandler;
}

void DogeSoaker::test()
{
	testPacketParsing(":zelazny.freenode.net CAP * LS :account-notify extended-join identify-msg multi-prefix sasl");
	testPacketParsing(":heathkid!~heathkid@unaffiliated/heathkid PRIVMSG #dogecoin :nice!");
	testPacketParsing(":ChanServ!ChanServ@services. MODE #shibesgonewild +v raedchen");
	testPacketParsing(":zelazny.freenode.net PONG zelazny.freenode.net :LAG1402163280980074");
	testPacketParsing(":zelazny.freenode.net CAP * LS :account-notify extended-join identify-msg multi-prefix sasl");
	testPacketParsing(":zelazny.freenode.net CAP Doge_Soaker2 ACK :sasl");
	testPacketParsing(":zelazny.freenode.net 352 Doge_Soaker2 #dogesoaker-ops2 ~doge_soak gateway/tor-sasl/sococpp/x-48425895 zelazny.freenode.net Doge_Soaker2 H@ :0 Doge Soaker 2");
	testPacketParsing("AUTHENTICATE +");
	testPacketParsing(":SoCo_cpp!~SoCo_cpp@gateway/tor-sasl/sococpp/x-48425895 NICK :SoCo_cpp__");
	testPacketParsing(":zelazny.freenode.net 311 Doge_Soaker4 SoCo_cpp ~SoCo_cpp gateway/tor-sasl/sococpp/x-48425895 * :SoCo cpp");
	testPacketParsing(":SoCo_cpp_hue!~SoCo_cpp@gateway/tor-sasl/sococpp/x-48425895 NICK :SoCo_cpp");
    testPacketParsing(":SoCo_cpp!~SoCo_cpp@2600:8804:8b00:a940:30b4:278f:1896:89ea PRIVMSG #dogecoin :IPv6 Test");
    testPacketParsing(":niven.freenode.net 311 SoCo_cpp ipv6user ~ipv6user 2600:8804:8b00:a940:30b4:278f:1896:89ea * :Such Ipv6 User");
}

void DogeSoaker::testPacketParsing(QString msg)
{
	IrcPacket packet(0, msg);
	DebugLog(dlDebug, "testPacketParsing testing: %s", qPrintable(packet.packet()));
	DebugLog(dlDebug, "  _Nick: '%s'", qPrintable(packet.nick().toString()));
	DebugLog(dlDebug, "  _prefix: '%s'", qPrintable(packet.prefix().toString()));
	DebugLog(dlDebug, "  _command: '%s'", qPrintable(packet.command().toString()));
	DebugLog(dlDebug, "  _middle: '%s'", qPrintable(packet.middle().toString()));
	DebugLog(dlDebug, "  _params: '%s'", qPrintable(packet.params().toString()));
	DebugLog(dlDebug, "  _trailing: '%s'", qPrintable(packet.trailing().toString()));
	QStringList paramList = packet.paramList();
	DebugLog(dlDebug, "  paramList(%d): '%s'", paramList.size(), qPrintable(paramList.join(",")));

}

void DogeSoaker::ircWatchdogError(QString sErrorMsg)
{
    DebugLog(dlWarn, "DogeSoaker::ircWatchdogError: %s", qPrintable(sErrorMsg));
    debugDumpStatus();
    reconnect();
}
void DogeSoaker::lagAverage(double msLagAverage)
{
    if (_lagLimitBadLag != 0 && msLagAverage > (double)_lagLimitBadLag)
    {
        _badLagCount++;
        DebugLog(dlWarn, "DogeSoaker::lagAverage bad lad detected %0.2fms, Bad lag count now set to %d. (bad lag limit %d, max lag limit %d)", (float)msLagAverage, _badLagCount, _lagLimitBadLag, _lagLimitMaxLag);
    }
    else
    {
        if (_badLagCount)
            DebugLog(dlDebug, "DogeSoaker::lagAverage lag acceptable %0.2fms, Bad lag count was %d but now zero. (bad lag limit %d, max lag limit %d)", (float)msLagAverage, _badLagCount, _lagLimitBadLag, _lagLimitMaxLag);
        _badLagCount = 0;
    }
    if ((_lagLimitMaxLag != 0 && msLagAverage > (double)_lagLimitMaxLag) || (_lagLimitBadMax != 0 && _badLagCount > _lagLimitBadMax))
    {
        DebugLog(dlWarn, "DogeSoaker::lagAverage Ping lag reconnect. Current lag %0.2fms, Bad lag count %d (bad lag limit %d, max lag limit %d)", (float)msLagAverage, _badLagCount, _lagLimitBadLag, _lagLimitMaxLag);
        debugDumpStatus();
        _badLagCount = 0;
        reconnect();
    }
}
