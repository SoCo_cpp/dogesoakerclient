#ifndef DOGESOAKERCONFIGFILE_H
#define DOGESOAKERCONFIGFILE_H

#include <QObject>

class JsonConfigFile; // predeclared

class DogeSoakerConfigFile : public QObject
{
	Q_OBJECT
public:
	explicit DogeSoakerConfigFile(QObject *parent = 0, const QString& sFileName = QString());

	void setFileName(const QString& sFileName);
	const QString& fileName() const;

	bool fileExists() const;

	bool load(const QString& sFileName = QString());
	bool save(const QString& sFileName = QString());

protected:
	JsonConfigFile* _cfgFile;

	bool read();
	bool write();
signals:

public slots:

};

#endif // DOGESOAKERCONFIGFILE_H
