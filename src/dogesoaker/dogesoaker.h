#ifndef DOGESOAKER_H
#define DOGESOAKER_H

#include <QObject>
#include <QJsonObject>
#include "comm/irc/ircclient.h"
#include "comm/irc/ircclientlogger.h"
#include "comm/irc/ircclientrawlogger.h"
#include "src/dogesoaker/dogesoakerirchandler.h"
#include "os/unixsignalhandler.h"

#define C_DogeSoaker_SystemName			"Doge Soaker"
#define C_DogeSoaker_SystemNameShort	"dogesoaker"

#define C_DogeSoaker_Version_Major		0
#define C_DogeSoaker_Version_Minor      2

#define C_DogeSoaker_DebugLog_Filename		"dogesoaker.log"
#define C_DogeSoaker_Config_Filename		"dogesoaker.config"
#define C_DogeSoaker_IrcLog_Dir				"irclog"
#define C_DogeSoaker_IrcLog_RawLog_FileName	"rawlog.txt"

class QCoreApplication; // predeclared

class DogeSoaker : public UnixSignalHandler
{
	Q_OBJECT
public:
	const static int cNotReadyStatusTimerInterval	= 50; // msec
    const static int cMaximumLag                    = 2500; // msec
    const static int cBadLag                        = 1200; // msec
    const static int cMaxBadLags                    = 3;

	typedef enum {mStop = 0, mStart, mPause, mRecoverNick} BotMode;
	typedef enum {bNone = 0,
				  bWaitingConnect, bConnected, bConnectionLost,
				  bReady, bError} BotStatus;

	typedef enum {cNone = 0, cDisconnecting, cLostConnection, cDisconnected, cConnecting, cConnected, cLogin, cReady} ConnectionStatus;

	explicit DogeSoaker();
	~DogeSoaker();

    static DogeSoaker& instance();

	const IrcClient& irc() const;
	IrcClient& irc();

    void setLagLimits(int badLag, int badMax, int maxLag);
    int lagLimitBadLag() const;
    int lagLimitBadMax() const;
    int lagLimitMaxLag() const;

    QString dataPath() const;
	QString ircLogPath() const;
	QString debugLogFilename() const;
	QString configFilename() const;

	bool start(QCoreApplication* pApp);
	bool initDebugLog();
	bool initIrcLogging();

	void setBotMode(BotMode botMode);
	const BotMode& botMode() const;
	QString botModeString() const;

	void setConnectionStatus(ConnectionStatus connectionStatus);
	const ConnectionStatus& connectionStatus() const;
	QString connectionStatusString() const;

	void setBotStatus(BotStatus botStatus);
	const BotStatus& botStatus() const;
	QString botStatusString() const;

	bool isConnected() const;
	bool isDisconnected(bool bFullDisconnect = false) const;
	bool isReady() const;

	void handleNickInUse();

	bool loadConfig();
	bool saveConfig();
	void defaultConfig();

	void setRawIrcLogEnabled(bool enabled = true);
	bool isRawIrcLogEnabled() const;

	void debugDumpStatus();

	void checkMode();

	const QString& embGetNick();
	void embChanMsg(const QString& sChannelName, const QString& sMessage);
	void embPrivMsg(const QString& sToNick, const QString& sMessage);
	void embCommand(const QString& sMessage);

	DogeSoakerIrcHandler& ircHandler();
	const DogeSoakerIrcHandler& ircHandler() const;

	void test();
	void testPacketParsing(QString msg);

protected:
	QCoreApplication* _pApp;
	BotMode _botMode;
	BotStatus _botStatus;
	ConnectionStatus _connectionStatus;
    IrcClient _irc;
	IrcClientLogger _ircLogger;
	IrcClientRawLogger _ircRawLogger;
	QString _versionString;
    QTimer _statusTimer;
	DogeSoakerIrcHandler _ircHandler;
	QTimer _recoverNickTimer;
	int _recoverNickStep;
    int _badLagCount;
    int _lagLimitBadLag;
    int _lagLimitBadMax;
    int _lagLimitMaxLag;

	virtual void unixSignalEvent(int sigNum); // inherited from UnixSignalHandler

signals:
	void botModeChanged();
	void connectionStatusChanged();
	void botStatusChanged();

public slots:
	void start();
	void stop();
	void pause();
	void resume();
	void reconnect();

	void applicationAboutToQuit();
	void subsystemStatusChanged();
	void statusTimerTick();
	void nickRecoveryTimerTick();
    void ircWatchdogError(QString sErrorMsg);
    void lagAverage(double msLagAverage);
};

#endif // DOGESOAKER_H
