#include "src/dogesoaker/dogesoakerconfigfile.h"
#include "src/dogesoaker/dogesoaker.h"
#include "file/jsonconfigfile.h"
#include "logging/debuglogger.h"

DogeSoakerConfigFile::DogeSoakerConfigFile(QObject* parent /*= 0*/, const QString& sFileName /*= QString()*/) :
	QObject(parent),
	_cfgFile(0)
{
	_cfgFile = new JsonConfigFile(this, sFileName, false/*compactJson*/);
}

void DogeSoakerConfigFile::setFileName(const QString& sFileName)
{
	_cfgFile->setFileName(sFileName);
}

const QString& DogeSoakerConfigFile::fileName() const
{
	return _cfgFile->fileName();
}

bool DogeSoakerConfigFile::fileExists() const
{
	return _cfgFile->exists();
}

bool DogeSoakerConfigFile::load(const QString& sFileName /*= QString()*/)
{
	if (!sFileName.isEmpty())
		setFileName(sFileName);
	if (!_cfgFile->load())
	{
		DebugLog(dlWarn, "DogeSoakerConfigFile::load cfgFile load failed");
		return false;
	}
	if (!read())
	{
		DebugLog(dlWarn, "DogeSoakerConfigFile::load read failed");
		return false;
	}
	return true;
}

bool DogeSoakerConfigFile::save(const QString& sFileName /*= QString()*/)
{
	_cfgFile->clear();
	if (!write())
	{
		DebugLog(dlWarn, "DogeSoakerConfigFile::save write failed");
		return false;
	}
	if (!sFileName.isEmpty())
		setFileName(sFileName);
	if (!_cfgFile->save())
	{
		DebugLog(dlWarn, "DogeSoakerConfigFile::save cfgFile save failed");
		return false;
	}
	return true;
}

bool DogeSoakerConfigFile::read()
{
	IrcClientConfig& cfg = DogeSoaker::instance().irc().config();

	if (!_cfgFile->findSection("DogeSoakerClient", 0))
	{
		DebugLog(dlWarn, "DogeSoakerConfigFile::read DogeSoakerConfigFile data not found");
		return false;
	}

    if (!_cfgFile->contains("bot-module-name"))
    {
        DebugLog(dlWarn, "DogeSoakerConfigFile::read bot-module-name specification not found");
        return false;
    }
    DogeSoaker::instance().ircHandler().setBotModuleName(_cfgFile->value("bot-module-name").toString().trimmed());

    if (!_cfgFile->contains("export-module-name"))
    {
        DebugLog(dlWarn, "DogeSoakerConfigFile::read bot-module-name specification not found");
        return false;
    }
    DogeSoaker::instance().ircHandler().setExportModuleName(_cfgFile->value("export-module-name").toString().trimmed());

	if (!_cfgFile->findSection("irc", 0))
	{
		DebugLog(dlWarn, "DogeSoakerConfigFile::read irc data not found");
		return false;
	}

    if (_cfgFile->contains("lag-limit-bad-lag") && _cfgFile->contains("lag-limit-bad-max") && _cfgFile->contains("lag-limit-max-lag"))
        DogeSoaker::instance().setLagLimits(_cfgFile->value("lag-limit-bad-lag").toInt(), _cfgFile->value("lag-limit-bad-max").toInt(), _cfgFile->value("lag-limit-max-lag").toInt());

    if (!_cfgFile->findSection("irc-server", 0))
	{
		DebugLog(dlWarn, "DogeSoakerConfigFile::read irc-server data not found");
		return false;
	}

	cfg.serverConfig().setHostName(			_cfgFile->value("host").toString());
	cfg.serverConfig().setHostPort((quint16)_cfgFile->value("port").toInt());
	cfg.serverConfig().setUseSsl(			_cfgFile->value("use-ssl").toBool());
	cfg.serverConfig().setIgnoreInvalidSsl(	_cfgFile->value("ignore-ssl-invalid").toBool());

	if (!_cfgFile->findSection("irc-server-proxy", 0))
	{
		DebugLog(dlWarn, "DogeSoakerConfigFile::read irc-server-proxy data not found");
		return false;
	}

	cfg.serverConfig().proxyConfig().setEnabled(_cfgFile->value("enabled").toBool());
	cfg.serverConfig().proxyConfig().setType((QNetworkProxy::ProxyType)_cfgFile->value("type").toInt());
	cfg.serverConfig().proxyConfig().setHostName(_cfgFile->value("host").toString());
	cfg.serverConfig().proxyConfig().setHostPort((quint16)_cfgFile->value("port").toInt());
    cfg.serverConfig().proxyConfig().setPromptCredentials(_cfgFile->value("prompt").toBool());
    cfg.serverConfig().proxyConfig().setUser(_cfgFile->value("user").toString());
	cfg.serverConfig().proxyConfig().setPassword(_cfgFile->value("password").toString());

	_cfgFile->popSection(); // irc-server-proxy
	_cfgFile->popSection(); // irc-server

	if (!_cfgFile->findSection("irc-account", 0))
	{
		DebugLog(dlWarn, "DogeSoakerConfigFile::read irc-server-proxy data not found");
		return false;
	}

	cfg.accountConfig().setNick(_cfgFile->value("nick").toString());
	cfg.accountConfig().setUser(_cfgFile->value("user").toString());
	cfg.accountConfig().setRealName(_cfgFile->value("realname").toString());
	cfg.accountConfig().setNickservPassword(_cfgFile->value("nickserv-password").toString());
	cfg.accountConfig().setUseSASL(_cfgFile->value("use-sasl").toBool());
	cfg.accountConfig().setSASLNick(_cfgFile->value("sasl-nick").toString());
	cfg.accountConfig().setSASLPassword(_cfgFile->value("sasl-password").toString());
	cfg.accountConfig().setAutoJoinChannels(_cfgFile->value("autojoin").toString());

	_cfgFile->popSection(); // irc-account

	_cfgFile->popSection(); // irc

	_cfgFile->popSection(); // DogeSoakerClient

	if (!_cfgFile->isBaseSection())
	{
		DebugLog(dlWarn, "DogeSoakerConfigFile::read read done, but config file not at base section");
		return false;
	}

	return true;
}

bool DogeSoakerConfigFile::write()
{
	IrcClientConfig& cfg = DogeSoaker::instance().irc().config();

	_cfgFile->pushSection("DogeSoakerClient", 0);
    _cfgFile->setValue("bot-module-name", DogeSoaker::instance().ircHandler().botModuleName());
    _cfgFile->setValue("export-module-name", DogeSoaker::instance().ircHandler().exportModuleName());
    _cfgFile->pushSection("irc", 0);
    _cfgFile->setValue("lag-limit-bad-lag", DogeSoaker::instance().lagLimitBadLag());
    _cfgFile->setValue("lag-limit-bad-max", DogeSoaker::instance().lagLimitBadMax());
    _cfgFile->setValue("lag-limit-max-lag", DogeSoaker::instance().lagLimitMaxLag());
    _cfgFile->pushSection("irc-server", 0);
		_cfgFile->setValue("host", cfg.serverConfig().hostName());
		_cfgFile->setValue("port", (int)cfg.serverConfig().hostPort());
		_cfgFile->setValue("use-ssl", cfg.serverConfig().useSsl());
		_cfgFile->setValue("ignore-ssl-invalid", cfg.serverConfig().ignoreInvalidSsl());

		_cfgFile->pushSection("irc-server-proxy", 0);
			_cfgFile->setValue("enabled", cfg.serverConfig().proxyConfig().isEnabled());
			_cfgFile->setValue("type", (int)cfg.serverConfig().proxyConfig().type());
			_cfgFile->setValue("host", cfg.serverConfig().proxyConfig().hostName());
			_cfgFile->setValue("port", (int)cfg.serverConfig().proxyConfig().hostPort());
			_cfgFile->setValue("user", cfg.serverConfig().proxyConfig().user());
			_cfgFile->setValue("password", cfg.serverConfig().proxyConfig().password());
			_cfgFile->setValue("prompt", cfg.serverConfig().proxyConfig().promptCredentials());
		_cfgFile->popSection(); // irc-server-proxy
	_cfgFile->popSection(); // irc-server

	_cfgFile->pushSection("irc-account", 0);
		_cfgFile->setValue("nick", cfg.accountConfig().nick());
		_cfgFile->setValue("user", cfg.accountConfig().user());
		_cfgFile->setValue("realname", cfg.accountConfig().realName());
		_cfgFile->setValue("nickserv-password", cfg.accountConfig().nickservPassword());
		_cfgFile->setValue("use-sasl", cfg.accountConfig().useSASL());
		_cfgFile->setValue("sasl-nick", cfg.accountConfig().saslNick());
		_cfgFile->setValue("sasl-password", cfg.accountConfig().saslPassword());
		_cfgFile->setValue("autojoin", cfg.accountConfig().autoJoinChannels());
	_cfgFile->popSection(); // irc-account

	_cfgFile->popSection(); // irc
	_cfgFile->popSection(); // DogeSoakerClient

	return true;
}
