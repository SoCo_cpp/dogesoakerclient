#ifndef DOGESOAKERIRCHANDLER_H
#define DOGESOAKERIRCHANDLER_H

#include <QObject>
#include <Python.h>
#include <pythread.h>
#include <QList>
#include <QTimer>

class DogeSoaker; // predeclared
class DogeSoakerIrcHandler : public QObject
{
	Q_OBJECT
public:


	explicit DogeSoakerIrcHandler(DogeSoaker& dogeSoaker);
	~DogeSoakerIrcHandler();

    void setBotModuleName(const QString& name);
    const QString& botModuleName() const;

    void setExportModuleName(const QString& name);
    const QString& exportModuleName() const;

	bool lock();
	bool unlock();

	bool lockSendPy();
	bool unlockSendPy();

	bool lockRecPy();
	bool unlockRecPy();

	bool init();
	void release();
	bool reload();

	const QString& embGetNick();
	void embChanMsg(const QString& sChannelName, const QString& sMessage);
	void embPrivMsg(const QString& sToNick, const QString& sMessage);
	void embCommand(const QString& sMessage);
	void embClientCommand(const QString& sMessage);

protected:

	DogeSoaker& _dogeSoaker;
	bool _initted;
    QString _botModuleName;
    QString _exportModuleName;
	QTimer _stepPyThreadsTimer;

	PyObject* _pyDogeSoakerModule;
	PyObject* _pyEmbIrcModule;

	PyObject* _pyDSInit;
	PyObject* _pyDSUnload;
	PyObject* _pyDSHandlePrivMsg; // FromUser, ToUser, Message
	PyObject* _pyDSHandleChannelMessage; // ChannelName, UserName, Message
	PyObject* _pyDSHandleWhoisAuth; // CurrNick, AuthNick
	PyObject* _pyDSHandleWhoisNameLine; // Nick, HostUser, Host, RealName
	PyObject* _pyDSHandleWhoisEnd; // Nick
	PyObject* _pyDSHandleJoin; // Nick, Host, Channel
	PyObject* _pyDSHandlePart; // Nick, Host, ChannelName, Reason
	PyObject* _pyDSHandleQuit; //  Nick, Host, Reason
	PyObject* _pyDSHandleKick; //  Nick, ChannelName, Reason, KickerNick
	PyObject* _pyDSHandleNickChanged; // OldNick, Host, NewNick

	typedef struct
	{
		PyObject** pyFunc;
		const char* funcName;
	} Py_DSFunctionDef;

	Py_DSFunctionDef Py_DSFunctionDefs[16];
	int Py_DSFunctionDefCount;

	void appendPyDSFunctionDef(PyObject** pyFunc, const char* funcName);
	bool initPyDSFunctions();
	void releasePyDSFunctions();

	int nickInUseRetryCount;

signals:
	void signalClientCommand(QString sMessage);

public slots:
	void receivedIrcHanlderKick(QString nick, QString user, QString channel, QString target, QString message);
	void receivedIrcHanlderJoin(QString nick, QString user, QString channel);
	void receivedIrcHanlderPart(QString nick, QString user, QString channel, QString message);
	void receivedIrcHanlderQuit(QString nick, QString user, QString message);
	void receivedIrcHanlderNick(QString nick, QString user,QString newNick);
	void receivedIrcHanlderPrivmsg(QString nick, QString user, QString target, QString message);

	void receivedIrcHandlerWhoisUser(QString nick, QString user, QString host, QString realName);
	void receivedIrcHandlerWhoisAuth(QString nick, QString authNick, QString message);
	void receivedIrcHandlerWhoisEnd(QString nick, QString message);

	void receivedIrcHandlerNickInUse(QString nick, QString message);
	void receivedIrcHandlerAuthFailed(QString nick, QString message);

	void stepPyThreadsTimerTick();

	void onDelayedReload();
	void handleClientCommand(QString sMessage);
};

#endif // DOGESOAKERIRCHANDLER_H
