#include "src/dogesoaker/dogesoakerirchandler.h"
#include "comm/irc/irchandler.h"
#include "comm/irc/ircclient.h"
#include "logging/debuglogger.h"
#include "src/dogesoaker/dogesoaker.h"
#include "src/dogesoaker/embedirc.h"
#include <QFile>

DogeSoakerIrcHandler::DogeSoakerIrcHandler(DogeSoaker& dogeSoaker) :
	QObject(0/*parent*/),
	_dogeSoaker(dogeSoaker),
	_initted(false),
    _botModuleName("dogesoaker"),
    _exportModuleName("embirc"),
	_pyDogeSoakerModule(NULL),
	_pyEmbIrcModule(NULL)

{
	nickInUseRetryCount = 0;
	Py_DSFunctionDefCount = 0;

	appendPyDSFunctionDef(&_pyDSInit,					"DS_Init");
	appendPyDSFunctionDef(&_pyDSUnload,					"DS_Unload");
	appendPyDSFunctionDef(&_pyDSHandlePrivMsg,			"DS_HandlePrivMsg");
	appendPyDSFunctionDef(&_pyDSHandleChannelMessage,	"DS_HandleChannelMessage");
	appendPyDSFunctionDef(&_pyDSHandleWhoisAuth,		"DS_HandleWhoisAuth");
	appendPyDSFunctionDef(&_pyDSHandleWhoisNameLine,	"DS_HandleWhoisNameLine");
	appendPyDSFunctionDef(&_pyDSHandleWhoisEnd,			"DS_HandleWhoisEnd");
	appendPyDSFunctionDef(&_pyDSHandleJoin,				"DS_HandleJoin");
	appendPyDSFunctionDef(&_pyDSHandlePart,				"DS_HandlePart");
	appendPyDSFunctionDef(&_pyDSHandleQuit,				"DS_HandleQuit");
	appendPyDSFunctionDef(&_pyDSHandleKick,				"DS_HandleKick");
	appendPyDSFunctionDef(&_pyDSHandleNickChanged,		"DS_HandleNickChanged");

	for (int idx = 0;idx < Py_DSFunctionDefCount;idx++)
		(*Py_DSFunctionDefs[idx].pyFunc) = NULL;

	_stepPyThreadsTimer.setInterval(100);

	connect(this, SIGNAL(signalClientCommand(QString)), this, SLOT(handleClientCommand(QString)));
	connect(&_stepPyThreadsTimer, SIGNAL(timeout()), this, SLOT(stepPyThreadsTimerTick()));
}

DogeSoakerIrcHandler::~DogeSoakerIrcHandler()
{
}

void DogeSoakerIrcHandler::appendPyDSFunctionDef(PyObject** pyFunc, const char* funcName)
{
	Py_DSFunctionDefs[Py_DSFunctionDefCount].pyFunc = pyFunc;
	Py_DSFunctionDefs[Py_DSFunctionDefCount].funcName = funcName;
	Py_DSFunctionDefCount++;
}

void DogeSoakerIrcHandler::setBotModuleName(const QString& name)
{
    _botModuleName = name;
}

const QString& DogeSoakerIrcHandler::botModuleName() const
{
    return _botModuleName;
}

void DogeSoakerIrcHandler::setExportModuleName(const QString& name)
{
    _exportModuleName = name;
}

const QString& DogeSoakerIrcHandler::exportModuleName() const
{
    return _exportModuleName;
}

void DogeSoakerIrcHandler::receivedIrcHanlderKick(QString nick, QString user, QString channel, QString target, QString message)
{
	Q_UNUSED(message);
	//DebugLog(dlDebug, "DogeSoakerIrcHandler::receivedIrcHanlderKick nick: %s, user: %s, channel: %s, Target: %s, Message: %s", qPrintable(nick), qPrintable(user), qPrintable(channel), qPrintable(target), qPrintable(message));
	if (!_initted)
	{
		qDebug() << "DogeSoaker IRC Kick received - not initted";
		DebugLog(dlError, "DogeSoakerIrcHandler::receivedIrcHanlderKick not initted");
		return;
	}
	PyObject* pArgs = PyTuple_New(4);
	PyTuple_SetItem(pArgs, 0, PyString_FromString(target.toUtf8().data()));
	PyTuple_SetItem(pArgs, 1, PyString_FromString(channel.toUtf8().data()));
	PyTuple_SetItem(pArgs, 2, PyString_FromString(message.toUtf8().data()));
	PyTuple_SetItem(pArgs, 3, PyString_FromString(QString(nick + " " + user).toUtf8().data()));
	PyObject_CallObject(_pyDSHandleKick, pArgs);
	Py_DECREF(pArgs);
}

void DogeSoakerIrcHandler::receivedIrcHanlderJoin(QString nick, QString user, QString channel)
{
	//DebugLog(dlDebug, "DogeSoakerIrcHandler::receivedIrcHanlderJoin nick: %s, user: %s, channel: %s", qPrintable(nick), qPrintable(user), qPrintable(channel));
	if (!_initted)
	{
		qDebug() << "DogeSoaker IRC Join received - not initted";
		DebugLog(dlError, "DogeSoakerIrcHandler::receivedIrcHanlderJoin not initted");
		return;
	}
	PyObject* pArgs = PyTuple_New(3);
	PyTuple_SetItem(pArgs, 0, PyString_FromString(nick.toUtf8().data()));
	PyTuple_SetItem(pArgs, 1, PyString_FromString(user.toUtf8().data()));
	PyTuple_SetItem(pArgs, 2, PyString_FromString(channel.toUtf8().data()));
	PyObject_CallObject(_pyDSHandleJoin, pArgs);
	Py_DECREF(pArgs);
}

void DogeSoakerIrcHandler::receivedIrcHanlderPart(QString nick, QString user, QString channel, QString message)
{
	//DebugLog(dlDebug, "DogeSoakerIrcHandler::receivedIrcHanlderPart nick: %s, user: %s, channel: %s, message: %s", qPrintable(nick), qPrintable(user), qPrintable(channel), qPrintable(message));
	if (!_initted)
	{
		qDebug() << "DogeSoaker IRC Part received - not initted";
		DebugLog(dlError, "DogeSoakerIrcHandler::receivedIrcHanlderPart not initted");
		return;
	}
	PyObject* pArgs = PyTuple_New(4);
	PyTuple_SetItem(pArgs, 0, PyString_FromString(nick.toUtf8().data()));
	PyTuple_SetItem(pArgs, 1, PyString_FromString(user.toUtf8().data()));
	PyTuple_SetItem(pArgs, 2, PyString_FromString(channel.toUtf8().data()));
	PyTuple_SetItem(pArgs, 3, PyString_FromString(message.toUtf8().data()));
	PyObject_CallObject(_pyDSHandlePart, pArgs);
	Py_DECREF(pArgs);
}

void DogeSoakerIrcHandler::receivedIrcHanlderQuit(QString nick, QString user, QString message)
{
    //DebugLog(dlDebug, "DogeSoakerIrcHandler::receivedIrcHanlderQuit (lengths) nick: %d, user: %d, message: %d", nick.size(), user.size(), message.size());
    if (nick.size() > 100 || user.size() > 100 || message.size() > 200)
    {
        nick.truncate(100);
        user.truncate(100);
        message.truncate(200);
        DebugLog(dlDebug, "DogeSoakerIrcHandler::receivedIrcHanlderQuit length anomoly detected, truncating values to 100/200 characters");
    }
    //DebugLog(dlDebug, "DogeSoakerIrcHandler::receivedIrcHanlderQuit nick: %s, user: %s, message: %s", qPrintable(nick), qPrintable(user), qPrintable(message));
	if (!_initted)
	{
		qDebug() << "DogeSoaker IRC Quit received - not initted";
		DebugLog(dlError, "receivedIrcHanlderQuit::receivedIrcHanlderPart not initted");
		return;
	}
	PyObject* pArgs = PyTuple_New(3);
	PyTuple_SetItem(pArgs, 0, PyString_FromString(nick.toUtf8().data()));
	PyTuple_SetItem(pArgs, 1, PyString_FromString(user.toUtf8().data()));
	PyTuple_SetItem(pArgs, 2, PyString_FromString(message.toUtf8().data()));
	PyObject_CallObject(_pyDSHandleQuit, pArgs);
	Py_DECREF(pArgs);
}

void DogeSoakerIrcHandler::receivedIrcHanlderNick(QString nick, QString user, QString newNick)
{
	//DebugLog(dlDebug, "DogeSoakerIrcHandler::receivedIrcHanlderNick old nick: %s, user: %s, new nick: %s", qPrintable(nick), qPrintable(user), qPrintable(newNick));
	if (!_initted)
	{
		qDebug() << "DogeSoaker IRC Quit NickChange - not initted";
		DebugLog(dlError, "receivedIrcHanlderQuit::receivedIrcHanlderNick not initted");
		return;
	}
	PyObject* pArgs = PyTuple_New(3);
	PyTuple_SetItem(pArgs, 0, PyString_FromString(nick.toUtf8().data()));
	PyTuple_SetItem(pArgs, 1, PyString_FromString(user.toUtf8().data()));
	PyTuple_SetItem(pArgs, 2, PyString_FromString(newNick.toUtf8().data()));
	PyObject_CallObject(_pyDSHandleNickChanged, pArgs);
	Py_DECREF(pArgs);
}

void DogeSoakerIrcHandler::receivedIrcHanlderPrivmsg(QString nick, QString user, QString target, QString message)
{
	Q_UNUSED(user);
	//DebugLog(dlDebug, "DogeSoakerIrcHandler::receivedIrcHanlderPrivmsg nick: %s user: %s, target: %s, is privmsg: %c, message: %s", qPrintable(nick), qPrintable(user), qPrintable(target), BoolYN(!target.startsWith(QChar('#'))), qPrintable(message));
	if (!_initted)
	{
		qDebug() << "DogeSoaker IRC Privmsg received - not initted";
		DebugLog(dlError, "DogeSoakerIrcHandler::receivedIrcHanlderPrivmsg not initted");
		return;
	}
	if (target.startsWith(QChar('#')))
	{
		// _pyDSHandleChannelMessage, ChannelName, UserName, Message
		PyObject* pArgs = PyTuple_New(3);
		PyTuple_SetItem(pArgs, 0, PyString_FromString(target.toUtf8().data()));
		PyTuple_SetItem(pArgs, 1, PyString_FromString(nick.toUtf8().data()));
		PyTuple_SetItem(pArgs, 2, PyString_FromString(message.toUtf8().data()));
		PyObject_CallObject(_pyDSHandleChannelMessage, pArgs);
		Py_DECREF(pArgs);
	}
	else
	{
		// _pyDSHandlePrivMsg, FromUser, ToUser, Message
		PyObject* pArgs = PyTuple_New(3);
		PyTuple_SetItem(pArgs, 0, PyString_FromString(nick.toUtf8().data()));
		PyTuple_SetItem(pArgs, 1, PyString_FromString(target.toUtf8().data()));
		PyTuple_SetItem(pArgs, 2, PyString_FromString(message.toUtf8().data()));
		PyObject_CallObject(_pyDSHandlePrivMsg, pArgs);
		Py_DECREF(pArgs);
	}
}


void DogeSoakerIrcHandler::receivedIrcHandlerWhoisUser(QString nick, QString user, QString host, QString realName)
{
	//DebugLog(dlDebug, "DogeSoakerIrcHandler::receivedIrcHandlerWhoisUser nick: %s user: %s, host: %s realname: %s", qPrintable(nick), qPrintable(user), qPrintable(host), qPrintable(realName));
	if (!_initted)
	{
		qDebug() << "DogeSoaker IRC Whois User received - not initted";
		DebugLog(dlError, "DogeSoakerIrcHandler::receivedIrcHandlerWhoisUser not initted");
		return;
	}
	PyObject* pArgs = PyTuple_New(4);
	PyTuple_SetItem(pArgs, 0, PyString_FromString(nick.toUtf8().data()));
	PyTuple_SetItem(pArgs, 1, PyString_FromString(user.toUtf8().data()));
	PyTuple_SetItem(pArgs, 2, PyString_FromString(host.toUtf8().data()));
	PyTuple_SetItem(pArgs, 3, PyString_FromString(realName.toUtf8().data()));
	PyObject_CallObject(_pyDSHandleWhoisNameLine, pArgs);
	Py_DECREF(pArgs);
}

void DogeSoakerIrcHandler::receivedIrcHandlerWhoisAuth(QString nick, QString authNick, QString message)
{
	Q_UNUSED(message);
	//DebugLog(dlDebug, "DogeSoakerIrcHandler::receivedIrcHandlerWhoisAuth nick: %s authNick: %s, message: %s", qPrintable(nick), qPrintable(authNick), qPrintable(message));
	if (!_initted)
	{
		qDebug() << "DogeSoaker IRC Whois Auth received - not initted";
		DebugLog(dlError, "DogeSoakerIrcHandler::receivedIrcHandlerWhoisAuth not initted");
		return;
	}
	PyObject* pArgs = PyTuple_New(2);
	PyTuple_SetItem(pArgs, 0, PyString_FromString(nick.toUtf8().data()));
	PyTuple_SetItem(pArgs, 1, PyString_FromString(authNick.toUtf8().data()));
	PyObject_CallObject(_pyDSHandleWhoisAuth, pArgs);
	Py_DECREF(pArgs);
}

void DogeSoakerIrcHandler::receivedIrcHandlerWhoisEnd(QString nick, QString message)
{
	Q_UNUSED(message);
	//DebugLog(dlDebug, "DogeSoakerIrcHandler::receivedIrcHandlerWhoisEnd nick: %s message: %s", qPrintable(nick), qPrintable(message));
	if (!_initted)
	{
		qDebug() << "DogeSoaker IRC Whois End received - not initted";
		DebugLog(dlError, "DogeSoakerIrcHandler::receivedIrcHandlerWhoisEnd not initted");
		return;
	}
	PyObject* pArgs = PyTuple_New(1);
	PyTuple_SetItem(pArgs, 0, PyString_FromString(nick.toUtf8().data()));
	PyObject_CallObject(_pyDSHandleWhoisEnd, pArgs);
	Py_DECREF(pArgs);
}

bool DogeSoakerIrcHandler::init()
{
	if (_initted)
	{
	   DebugLog(dlError, "DogeSoakerIrcHandler::init already initted");
	   return false;
	}
    Py_SetProgramName((char*)"DogeSoakerClient");
	Py_Initialize();
	PyEval_InitThreads();

    _pyEmbIrcModule = Py_InitModule(_exportModuleName.toUtf8().data(), EmbIrcMethods);
	if (!_pyEmbIrcModule)
	{
	   DebugLog(dlWarn, "DogeSoakerIrcHandler::init prepare EmbIrc Module failed");
	   return false;
	}

    PyObject* pName = PyString_FromString(_botModuleName.toUtf8().data());
	_pyDogeSoakerModule = PyImport_Import(pName);
	Py_DECREF(pName);

	if (!_pyDogeSoakerModule)
	{
		PyErr_Print();
        qDebug() << "Ensure python can find your module. Consider adding its path to the PYTHONPATH environmental variable.";
		DebugLog(dlDebug, "DogeSoakerIrcHandler::init validate module failed");
		return false;
	}

	if (!initPyDSFunctions())
	{
		DebugLog(dlWarn, "DogeSoakerIrcHandler::init initPyDSFunctions failed");
		return false;
	}

	PyObject_CallObject(_pyDSInit, NULL);

	_initted = true;

	connect(&_dogeSoaker.irc().handler(), SIGNAL(receivedNick(QString,QString,QString)), this, SLOT(receivedIrcHanlderNick(QString,QString,QString)));
	connect(&_dogeSoaker.irc().handler(), SIGNAL(receivedJoin(QString,QString,QString)), this, SLOT(receivedIrcHanlderJoin(QString,QString,QString)));
	connect(&_dogeSoaker.irc().handler(), SIGNAL(receivedPart(QString,QString,QString,QString)), this, SLOT(receivedIrcHanlderPart(QString,QString,QString,QString)));
	connect(&_dogeSoaker.irc().handler(), SIGNAL(receivedQuit(QString,QString,QString)), this, SLOT(receivedIrcHanlderQuit(QString,QString,QString)));
	connect(&_dogeSoaker.irc().handler(), SIGNAL(receivedKick(QString,QString,QString,QString,QString)), this, SLOT(receivedIrcHanlderKick(QString,QString,QString,QString,QString)));
	connect(&_dogeSoaker.irc().handler(), SIGNAL(receivedPrivmsg(QString,QString,QString,QString)), this, SLOT(receivedIrcHanlderPrivmsg(QString,QString,QString,QString)));

	connect(&_dogeSoaker.irc().handler(), SIGNAL(receivedWhoisUser(QString,QString,QString,QString)), this, SLOT(receivedIrcHandlerWhoisUser(QString,QString,QString,QString)));
	connect(&_dogeSoaker.irc().handler(), SIGNAL(receivedWhoisAuth(QString,QString,QString)), this, SLOT(receivedIrcHandlerWhoisAuth(QString,QString,QString)));
	connect(&_dogeSoaker.irc().handler(), SIGNAL(receivedWhoisEnd(QString,QString)), this, SLOT(receivedIrcHandlerWhoisEnd(QString,QString)));

	connect(&_dogeSoaker.irc().handler(), SIGNAL(receivedNickInUse(QString,QString)), this, SLOT(receivedIrcHandlerNickInUse(QString,QString)));
	connect(&_dogeSoaker.irc().handler(), SIGNAL(receivedAuthFailed(QString,QString)), this, SLOT(receivedIrcHandlerAuthFailed(QString,QString)));

	DebugLog(dlDebug, "DogeSoakerIrcHandler::init all succeeded");
	_stepPyThreadsTimer.start();
	return true;
}

bool DogeSoakerIrcHandler::initPyDSFunctions()
{
	PyObject* pyTmp;
	for (int idx = 0;idx < Py_DSFunctionDefCount;idx++)
	{
		pyTmp = PyObject_GetAttrString(_pyDogeSoakerModule, Py_DSFunctionDefs[idx].funcName);
		if (!pyTmp || !PyCallable_Check(pyTmp))
		{
			PyErr_Print();
			DebugLog(dlWarn, "DogeSoakerIrcHandler::initPyDSFunctions prepare python function binding to %s failed", Py_DSFunctionDefs[idx].funcName);
			return false;
		}
		(*Py_DSFunctionDefs[idx].pyFunc) = pyTmp;
	}
	return true;
}

void DogeSoakerIrcHandler::release()
{
	_stepPyThreadsTimer.stop();
	disconnect(&_dogeSoaker.irc().handler(), 0, this, 0);

	if (_initted)
		PyObject_CallObject(_pyDSUnload, NULL);

	releasePyDSFunctions();

	if (_pyEmbIrcModule)
	{
		Py_DECREF(_pyEmbIrcModule);
		_pyEmbIrcModule = NULL;
	}
	if (_pyDogeSoakerModule)
	{
		Py_DECREF(_pyDogeSoakerModule);
		_pyDogeSoakerModule = NULL;
	}
	Py_Finalize();
	_initted = false;
}

bool DogeSoakerIrcHandler::reload()
{
	DebugLog(dlDebug, "DogeSoakerIrcHandler::reload...");
	release();
	if (!init())
	{
		DebugLog(dlWarn, "DogeSoakerIrcHandler::reload init failed");
		return false;
	}
	DebugLog(dlInfo, "DogeSoakerIrcHandler::reload plugin reloaded");
	return true;
}

void DogeSoakerIrcHandler::onDelayedReload()
{
	if (!reload())
		DebugLog(dlDebug, "DogeSoakerIrcHandler::onDelayedReload reload failed");
}

void DogeSoakerIrcHandler::releasePyDSFunctions()
{
	for (int idx = 0;idx < Py_DSFunctionDefCount;idx++)
		if ( (*Py_DSFunctionDefs[idx].pyFunc) )
		{
			Py_XDECREF( (*Py_DSFunctionDefs[idx].pyFunc) );
			(*Py_DSFunctionDefs[idx].pyFunc) = NULL;
		}
}

void DogeSoakerIrcHandler::receivedIrcHandlerNickInUse(QString nick, QString message)
{
	DebugLog(dlDebug, "DogeSoakerIrcHandler::receivedIrcHandlerNickInUse nick: %s, message: %s", qPrintable(nick), qPrintable(message));
	_dogeSoaker.handleNickInUse();
}

void DogeSoakerIrcHandler::receivedIrcHandlerAuthFailed(QString nick, QString message)
{
	DebugLog(dlDebug, "DogeSoakerIrcHandler::receivedIrcHandlerAuthFailed nick: %s, message: %s", qPrintable(nick), qPrintable(message));
	_dogeSoaker.reconnect();
}

void DogeSoakerIrcHandler::stepPyThreadsTimerTick()
{
	Py_BEGIN_ALLOW_THREADS
	usleep(1);
	Py_END_ALLOW_THREADS
}

const QString& DogeSoakerIrcHandler::embGetNick()
{
	//qDebug() << "DogeSoakerIrcHandler::embGetNick";
	return _dogeSoaker.irc().config().accountConfig().nick();
}

void DogeSoakerIrcHandler::embChanMsg(const QString& sChannelName, const QString& sMessage)
{
	//qDebug() << "DogeSoakerIrcHandler::embChanMsg";
	Py_BEGIN_ALLOW_THREADS
	emit _dogeSoaker.irc().handler().sendPrivateMessage(sChannelName, sMessage);
	Py_END_ALLOW_THREADS
}

void DogeSoakerIrcHandler::embPrivMsg(const QString& sToNick, const QString& sMessage)
{
	//qDebug() << "DogeSoakerIrcHandler::embPrivMsg";
	Py_BEGIN_ALLOW_THREADS
	emit _dogeSoaker.irc().handler().sendPrivateMessage(sToNick, sMessage);
	Py_END_ALLOW_THREADS
}

void DogeSoakerIrcHandler::embCommand(const QString& sMessage)
{
	Py_BEGIN_ALLOW_THREADS
	//qDebug() << "DogeSoakerIrcHandler::embCommand " << sMessage;
	emit _dogeSoaker.irc().handler().send(sMessage);
	Py_END_ALLOW_THREADS
}

void DogeSoakerIrcHandler::embClientCommand(const QString& sMessage)
{
	//qDebug() << "DogeSoakerIrcHandler::embClientCommand" << sMessage;
	Py_BEGIN_ALLOW_THREADS
	emit signalClientCommand(sMessage);
	Py_END_ALLOW_THREADS
}

void DogeSoakerIrcHandler::handleClientCommand(QString sMessage)
{
	if (sMessage.toLower().startsWith("reconnect"))
	{
		_dogeSoaker.reconnect();
	}
	else if (sMessage.toLower().startsWith("reload"))
	{
		QTimer::singleShot(1000, this, SLOT(onDelayedReload()));
	}
	else if (sMessage.toLower().startsWith("rawlog"))
	{
		sMessage = sMessage.mid(6).trimmed();
		if (sMessage.toLower() == "on")
			_dogeSoaker.setRawIrcLogEnabled(true);
		else
			_dogeSoaker.setRawIrcLogEnabled(false);
	}
	else DebugLog(dlWarn, "DogeSoakerIrcHandler::handleClientCommand received unhandled command: %s", qPrintable(sMessage));
}
