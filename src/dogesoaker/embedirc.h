#ifndef EMBEDIRC_H
#define EMBEDIRC_H

#include <Python.h>
#include "src/dogesoaker/dogesoaker.h"

static PyObject* embirc_GetNick(PyObject* self, PyObject* args)
{
	Q_UNUSED(self);
	Q_UNUSED(args);
	//qDebug() << "embirc_GetNick triggered";
	QString sNick = DogeSoaker::instance().ircHandler().embGetNick();
	PyObject* psNick = PyString_FromString(sNick.toLatin1().data());
	return psNick;
}

static PyObject* embirc_ChanMsg(PyObject* self, PyObject* args)
{
	Q_UNUSED(self);
	char* strChannel;
	char* strMessage;
	//qDebug() << "embirc_ChanMsg triggered";
	if (PyArg_ParseTuple(args, "ss", &strChannel, &strMessage))
		DogeSoaker::instance().ircHandler().embChanMsg(QString(strChannel), QString(strMessage));
	return Py_BuildValue("");
}

static PyObject* embirc_PrivMsg(PyObject* self, PyObject* args)
{
	Q_UNUSED(self);
	char* strToNick;
	char* strMessage;
	//qDebug() << "embirc_PrivMsg triggered";
	if (PyArg_ParseTuple(args, "ss", &strToNick, &strMessage))
		DogeSoaker::instance().ircHandler().embPrivMsg(QString(strToNick), QString(strMessage));
	return Py_BuildValue("");
}

static PyObject* embirc_Command(PyObject* self, PyObject* args)
{
	Q_UNUSED(self);
	char* strMessage;
	//qDebug() << "embirc_Command triggered";
	if (PyArg_ParseTuple(args, "s", &strMessage))
		DogeSoaker::instance().ircHandler().embCommand(QString(strMessage));
	return Py_BuildValue("");
}

static PyObject* embirc_ClientCommand(PyObject* self, PyObject* args)
{
	Q_UNUSED(self);
	char* strMessage;
	if (PyArg_ParseTuple(args, "s", &strMessage))
		DogeSoaker::instance().ircHandler().embClientCommand(QString(strMessage));
	return Py_BuildValue("");
}

static PyMethodDef EmbIrcMethods[] = {
	{"GetNick",			(PyCFunction) embirc_GetNick,		METH_VARARGS, "GetNick"},
	{"ChanMsg",			(PyCFunction) embirc_ChanMsg,		METH_VARARGS, "ChanMsg"},
	{"PrivMsg",			(PyCFunction) embirc_PrivMsg,		METH_VARARGS, "PrivMsg"},
	{"Command",			(PyCFunction) embirc_Command,		METH_VARARGS, "Command"},
	{"ClientCommand",	(PyCFunction) embirc_ClientCommand,	METH_VARARGS, "ClientCommand"},
	{NULL, NULL, 0, NULL}
};

#endif // EMBEDIRC_H
